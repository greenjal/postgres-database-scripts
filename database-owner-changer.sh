#!/bin/bash

# Load config file
if test -f conf/database-owner-changer.conf ; then
    . conf/database-owner-changer.conf
fi

[ ! -z "$db_host" ] || read -p 'Database host [localhost]: ' db_host
db_host=${db_host:-5432}

[ ! -z "$db_port" ] || read -p 'Database port [5432]: ' db_port
db_port=${db_port:-5432}

[ ! -z "$db_username" ] || read -p 'Database username [postgres]: ' db_username
db_username=${db_username:-postgres}

[ ! -z "$db_password" ] || read -p 'Database password [postgres]: ' db_password
db_password=${db_password:-postgres}

[ ! -z "$db_password" ] || read -p 'Database password [postgres]: ' db_password
db_password=${db_password:-postgres}

[ ! -z "$db_name" ] || read -p "Db Name: " db_name
[ ! -z "$new_owner" ] || read -p "New Owner: " new_owner
[ ! -z "$multiple_space_separated_schema" ] || read -p "schemas seperated by space: " multiple_space_separated_schema

IFS=' ' read -ra schemas <<< "$multiple_space_separated_schema"

export PGPASSWORD=$db_password
for schema in "${schemas[@]}"
do
        echo "Changing owner for schema $schema"
        psql --host $db_host --port $db_port --username $db_username -c "alter schema \"$schema\" owner to $new_owner" $db_name;

        for tbl in `psql --host $db_host --port $db_port --username $db_username -qAt -c "select tablename from pg_tables where schemaname = '$schema';" $db_name` ;
                do  psql --host $db_host --port $db_port --username $db_username -c "alter table \"$schema\".\"$tbl\" owner to $new_owner" $db_name ;
        done;

        for tbl in `psql --host $db_host --port $db_port --username $db_username -qAt -c "select sequence_name from information_schema.sequences where sequence_schema = '$schema';" $db_name` ;
                do  psql --host $db_host --port $db_port --username $db_username -c "alter sequence \"$schema\".\"$tbl\" owner to $new_owner" $db_name ;
        done;

        for tbl in `psql --host $db_host --port $db_port --username $db_username -qAt -c "select table_name from information_schema.views where table_schema = '$schema';" $db_name` ;
                do  psql --host $db_host --port $db_port --username $db_username -c "alter view \"$schema\".\"$tbl\" owner to $new_owner" $db_name ;
        done
done
