#!/bin/bash

if [[ ! -f conf/backup-databases.conf ]]
then
    echo 'conf/backup-databases.conf file does not exists!!!'
    echo 'Please create a text file conf/backup-databases.conf with name of databases to backup on each line'
    exit 1
fi

### You can add data here directly and comment read lines to directly connect to server
if [[ ! -z conf/backup-servers.conf ]]; then
    . conf/backup-servers.conf
fi

[ ! -z "$ssh_user" ] || read -p "ssh username: " ssh_user
[ ! -z "$ssh_ip" ] || read -p "ssh ip: " ssh_ip
[ ! -z "$pg_user" ] || read -p "Enter database username: " pg_user
[ ! -z "$pg_password" ] || read -p "Enter Password for postgres user: " -s pg_password

bash backup-all-database.sh $ssh_user $ssh_ip $pg_user $pg_password
clear
echo "Backup Complete"
bash copy-database-backup.sh $ssh_user $ssh_ip
