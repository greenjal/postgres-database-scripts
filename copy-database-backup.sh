
#!/bin/bash
# echo "Enter Password for pathway user:"

#read -s sshpassword

ssh_user=$1
ssh_ip=$2

input="conf/backup-databases.conf"

#clear
echo "Copying Backup of all database"
while IFS= read -r line
do
    echo "##########################################################"
    echo "Copying backup of database: $line";
    echo "##########################################################"
    rsync -a -P $ssh_user@$ssh_ip:/tmp/.database-backups/$line.sql ./database-backups/
    clear
    echo "##########################################################"
    echo "Copying Backup complete: $line"
    echo "##########################################################"
    
done < "$input"

echo "Copy completed"
