#!/bin/bash

# Load config file
if test -f conf/database-restorer.conf ; then
    . conf/database-restorer.conf
fi

[ ! -z "$backup_file" ] || read -p 'Database file location: ' backup_file

[ ! -z "$db_name" ] || read -p 'Database name: ' db_name

[ ! -z "$db_host" ] || read -p 'Database host [localhost]: ' db_host
db_host=${db_host:-5432}

[ ! -z "$db_port" ] || read -p 'Database port [5432]: ' db_port
db_port=${db_port:-5432}

[ ! -z "$db_username" ] || read -p 'Database username [postgres]: ' db_username
db_username=${db_username:-postgres}

[ ! -z "$db_password" ] || read -p 'Database password [postgres]: ' db_password
db_password=${db_password:-postgres}



read -p 'Drop old database [y/N]:' drop_db

if [[ -z "$drop_db" || "$drop_db" =~ ^([nN][oO]|[nN])$ ]]; then
    read -p 'Create new database [y/N]:' create_db
fi

export PGPASSWORD=$db_password

if [[ "$drop_db" =~ ^([yY][eE][sS]|[yY])$ ]]
then
    echo "Dropping old database $db_name"
    psql --host $db_host --port $db_port --username $db_username -c "DROP DATABASE $db_name;"
    psql --host $db_host --port $db_port --username $db_username -c "CREATE DATABASE $db_name;"
fi

if [[ "$create_db" =~ ^([yY][eE][sS]|[yY])$ ]]
then
    echo "Creating new database $db_name"
    psql --host $db_host --port $db_port --username $db_username -c "CREATE DATABASE $db_name;"
fi


echo "Restoring database $db_name from $backup_file"
pg_restore --host $db_host --port $db_port --username $db_username --dbname "$db_name" --verbose "$backup_file"
