#!/bin/bash

# Load config file
if test -f conf/function-backuper.conf ; then
    . conf/function-backuper.conf
fi

[ ! -z "$output_file_name" ] || output_file_name=functions.sql

[ ! -z "$remote_db_name" ] || read -p 'Remote database name: ' remote_db_name


[ ! -z "$remote_db_host" ] || read -p 'Remote database host: ' remote_db_host

[ ! -z "$remote_db_port" ] || read -p 'Remote database port [5432]: ' remote_db_port
remote_db_port=${remote_db_port:-5432}

[ ! -z "$remote_db_username" ] || read -p 'Remote database username [postgres]: ' remote_db_username
remote_db_username=${remote_db_username:-postgres}

[ ! -z "$remote_db_password" ] || read -p 'Remote database password [postgres]: ' remote_db_password
remote_db_password=${remote_db_password:-postgres}

[ ! -z "$function_pattern" ] || read -p 'Function/Procedure pattern (WHERE clause): ' function_pattern



echo "Backuping function from db $remote_db_name on $remote_db_host"
export PGPASSWORD=$remote_db_password
psql -U $remote_db_username -h $remote_db_host -d $remote_db_name -p $remote_db_port -t -A -c "SELECT concat(pg_get_functiondef(f.oid),';') FROM pg_catalog.pg_proc f INNER JOIN pg_catalog.pg_namespace n ON (f.pronamespace = n.oid) $function_pattern" > $output_file_name
echo "Backup Completed"


[ ! -z "$restore_function" ] || read -p 'Restore functions [y/N]:' restore_function
if [[ "$restore_function" =~ ^([yY][eE][sS]|[yY])$ ]]
then
    echo "Restoring function from db $local_db_name on $local_db_host"
    [ ! -z "$local_db_name" ] || read -p 'Local database name [Same name as remote db]: ' local_db_name
    local_db_name=${local_db_name:-$remote_db_name}
    
    [ ! -z "$local_db_host" ] || read -p 'Local database host [localhost]: ' local_db_host
    local_db_host=${local_db_host:-localhost}
    
    [ ! -z "$local_db_port" ] || read -p 'Local database port [5432]: ' local_db_port
    local_db_port=${local_db_port:-5432}
    
    [ ! -z "$local_db_username" ] || read -p 'Local database username [postgres]: ' local_db_username
    local_db_username=${local_db_username:-postgres}
    
    [ ! -z "$local_db_password" ] || read -p 'Local database password [postgres]: ' local_db_password
    local_db_password=${local_db_password:-postgres}
    
    export PGPASSWORD=$local_db_password
    psql -h $local_db_host -U $local_db_username -p $local_db_port -f $output_file_name -d $local_db_name
    echo "Restore Completed"
fi

