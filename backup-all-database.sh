
#!/bin/bash


ssh_user=$1
ssh_ip=$2

pg_user=$3
pg_password=$4

input="conf/backup-databases.conf"

echo "Starting Backup of all database"
while IFS= read -r line
do
    echo "##########################################################"
    echo "Backuping database: $line";
    echo "##########################################################"
    ssh $ssh_user@$ssh_ip "bash -s" < backup-function.sh $pg_user $pg_password $line;
    echo "##########################################################"
    echo "Backup complete: $line";
    echo "##########################################################"
    
done < "$input"

echo "All backup completed"
exit 0
