# Postgres Database Tools & Scripts

This repo consists of some bash scripts that may be helpful to backup & restore postgres database. You can copy the conf.sample folder to conf and setup configurations for each tools.

Some of the tools with their basic descriptions and uses are as follows.

## Ssh backup

We do have tools like pg_dump and also tools like pgAdmin to backup postgres db. But the backup through these tools may be relatively slower specially if you are using ssh tunnel.

This tool backups list of database directly on the ssh server on /tmp/.database-backups and then copies the .sql backup to database-backups folder on your local machine.

All you need to do is add database names on conf/backup-databases.conf on each line then run `bash backup-starter.sh` and the script will do the rest.

You can also add server's configurations on conf/backup-servers.conf. Since the script will source the conf/backup-servers.conf, you can also setup multiple servers as another bash script as shown in sample conf file.

This script also uses some other helper scripts.

1. backup-all-database .sh: This script reads each line of conf/backup-databases.conf and backups database on server.

2. backup-function .sh: This script is copied to server and run on server to backup a db on server's /tmp/.database-backups location

3. copy-database-backup .sh: This script copies the database backup from server from /tmp/.database-backups to local machine's database-backups

***Use***

* Add database names in conf/backup-databases.conf
* Optionally add conf/backup-servers.conf for ssh server configuration or enter directly when script asks for input
* Run the script

    ```bash
    bash backup-starter.sh
    ```

## Function Backuper

If you have ever worked with postgres's functions or procedures and just wanted to backup just the functions then you may have noticed that pg_dump or pgAdmin is not much help as this cannot be done easily.

This tool uses a psql command and postgresql query to backup database functions into a file. You can define a WHERE clause to select functions or patterns of functions as shown in sample conf/function-backuper.sh

It can also restore the backuped function to another database. e.g. Backup functions from remote db and restore to local db.

***Use***

* Optionally define conf/function-backuper.conf
* Run the script
  
  ```bash
  bash function-backuper.sh
  ```

## Database Owner Changer

If you ever have to change the owner of an postgres database then you might know that just change the database owner is not enough. You have to change owner for each schema, view and tables which may not be possible if your database is huge. And even if your database has relatively small number of schema and table, it still is a tedious process.

This script will loop and change through all the tables, views, sequences of listed schemas.

***Use***

* Optionally define conf/database-owner-changer.conf
* Run the script
  
  ```bash
  bash database-owner-changer.sh
  ```

## Database Restorer

Not much special about this script. It will just pg_restore an backup file. But if you want to create new db to restore or drop old db before restoring a backup, this script will do it at once.
***Use***

* Optionally define conf/database-restorer.conf
* Run the script
  
  ```bash
  bash database-restorer.sh
  ```
