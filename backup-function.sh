#!/bin/bash

pg_user=$1
export PGPASSWORD=$2
if [[ ! -d /tmp/.database-backups ]]
then
    echo "Folder doesnot exists"
    mkdir -p /tmp/.database-backups
else
    echo "Folder exists"
fi

cd /tmp/.database-backups
pg_dump --file "$3.sql" --host "localhost" --port "5432" --username "$1" --verbose --format=c --blobs "$3"